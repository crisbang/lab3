<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Order;
use App\Form\Type\Client\ClientType;
use App\Repository\ClientRepository;
use App\Repository\WorkerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{


    /**
     * @Route("/main", name="main_page", methods={"GET", "POST"})
     */
    public function client(Request $request, WorkerRepository $repository, EntityManagerInterface $em): Response
    {
        $client = new Client();
        $worker = $repository->find(1);
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($client);
            $em->flush();

            $date = 'Не согласована';
            $order = new Order();
            $order
                ->setDate($date)
                ->setClient($client)
                ->setOrworker($worker);

            $em->persist($order);
            $em->flush();
        }


        return $this->render('home-page/main.html.twig', ['client_form' => $form->createView(),]);

    }
}
