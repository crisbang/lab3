<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClientController extends AbstractController
{
    /**
     * @Route("/clients/create", name="client_create", methods={"GET"})
     */
    public function create(ClientRepository $repository): Response
    {
        $client = new Client();
        $client
            ->setFio('Веснян Пантелеймонович Сомородский')
            ->setPhone('89340982368')
            ->setEmail('ves@gmail.com');

        $repository->save($client, true);
        return new Response();
    }

    /**
     * @Route("/clients/update", name="client_update", methods={"GET"})
     */
    public function update(ClientRepository $repository): Response
    {
        $client = $repository->find(1);

        if ($client !== null) {
            $client->setPhone('89998887766');
            $repository->save($client, true);
        }

        return new Response();
    }

    /**
     * @Route("/clients/remove", name="client_remove", methods={"GET"})
     */
    public function remove(ClientRepository $repository): Response
    {
        $client = $repository->find(2);

        if ($client !== null) {
            $repository->remove($client, true);
        }

        return new Response();
    }

}