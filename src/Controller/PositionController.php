<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Position;
use App\Entity\Service;
use App\Entity\Worker;
use App\Repository\PositionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PositionController extends AbstractController
{
    /**
     * @Route("/positions/create", name="position_create", methods={"GET"})
     */
    public function create(PositionRepository $repository): Response
    {
        $position = new Position();
        $position
            ->setTitle('Тату-мастер');

        $repository->save($position, true);
        return new Response();
    }

    /**
     * @Route("/positions/update", name="position_update", methods={"GET"})
     */
    public function update(PositionRepository $repository): Response
    {
        $position = $repository->find(1);

        if ($position !== null) {
            $position->setTitle('Пирсинг-мастер');
            $repository->save($position, true);
        }

        return new Response();
    }

    /**
     * @Route("/positions/remove", name="position_remove", methods={"GET"})
     */
    public function remove(PositionRepository $repository): Response
    {
        $position = $repository->find(2);

        if ($position !== null) {
            $repository->remove($position, true);
        }

        return new Response();
    }

    //add row to position_service
    /**
     * @Route("/positions/addservice", name="position_service", methods={"GET"})
     */
    public function addService(EntityManagerInterface $em): Response
    {
        $position = $em->getRepository(Position::class)->find(1);
        $service = $em->getRepository(Service::class)->find(1);

        $position->addService($service);

        $em->persist($position);
        $em->flush();

        return new Response();
    }

    //output services of the position
    /**
     * @Route("/positions/{id}/services", name="position_services", methods={"GET"})
     */
    public function getPositionServices(int $id, EntityManagerInterface $em): Response
    {
        $position = $em->getRepository(Position::class)->find($id);

        foreach ($position->getServices() as $service){
            dd($service);
        }

        return new Response();
    }

    //add worker
    /**
     * @Route("/positions/{id}/workers", name="position_workers", methods={"GET"})
     */
    public function createWorker(int $id, EntityManagerInterface $em): Response
    {
        $position = $em->getRepository(Position::class)->find($id);

        if ($position !== null) {
            $worker  = new Worker();
            $worker
                ->setFio('Даромысл Добровитович Справедливый')
                ->setPhone('80001112233')
                ->setPosition($position);
            $em->persist($worker);
            $em->flush();

            return new Response();
        }

        return new Response('Position not found', 404);
    }

    //remove worker
    /**
     * @Route("/positions/{id}/workers/remove", name="position_workers_remove", methods={"GET"})
     */
    public function removeWorker(int $id, EntityManagerInterface $em): Response
    {
        $position = $em->getRepository(Position::class)->find($id);

        if ($position !== null) {
            $workers= $position->getWorker();
            $position->removeWorker($workers->first());

            $em->persist($position);
            $em->flush();

            return new Response('Account was remove');
        }

        return new Response('Position not found', 404);
    }

}