<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\Service;
use App\Entity\Worker;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class OrderController extends AbstractController
{
    /**
     * @Route("/orders/{idclient}/{idorworker}/create", name="create_order", methods={"GET"})
     */
    public function createOrder(int $idclient, int $idorworker, EntityManagerInterface $em): Response
    {
        $client = $em->getRepository(Client::class)->find($idclient);
        $worker = $em->getRepository(Worker::class)->find($idorworker);

        if ($client !== null and $worker !== null) {
            $order = new Order();
            $order
                ->setDate('2023-01-01, 15:00')
                ->setCost(2000)
                ->setClient($client)
                ->setOrworker($worker);
            $em->persist($order);
            $em->flush();

            return new Response('Ok');
        }

        return new Response('Client or Worker not found', 404);
    }

    /**
     * @Route("/order/update", name="order_update", methods={"GET"})
     */
    public function update(OrderRepository $repository): Response
    {
        $order = $repository->find(1);

        if ($order !== null) {
            $order->setDate('2021-01-05, 9:00');
            $repository->save($order, true);
        }

        return new Response();
    }

    /**
     * @Route("/orders/remove", name="orders_remove", methods={"GET"})
     */
    public function remove(OrderRepository $repository): Response
    {
        $order = $repository->find(2);

        if ($order !== null) {
            $repository->remove($order, true);
        }

        return new Response();
    }


    //add row to order_product
    /**
     * @Route("/orders/addproduct", name="order_product", methods={"GET"})
     */
    public function addProduct(EntityManagerInterface $em): Response
    {
        $order = $em->getRepository(Order::class)->find(1);
        $product = $em->getRepository(Product::class)->find(1);

        $order->addListproduct($product);

        $em->persist($product);
        $em->flush();

        return new Response();
    }

    //output products of the order
    /**
     * @Route("/orders/{id}/products", name="orders_products", methods={"GET"})
     */
    public function getOrderProducts(int $id, EntityManagerInterface $em): Response
    {
        $order = $em->getRepository(Order::class)->find($id);

        foreach ($order->getListproducts() as $product){
            dd($product);
        }

        return new Response();
    }

    //add row to order_service
    /**
     * @Route("/orders/addservice", name="order_service", methods={"GET"})
     */
    public function addService(EntityManagerInterface $em): Response
    {
        $order = $em->getRepository(Order::class)->find(1);
        $service = $em->getRepository(Service::class)->find(1);

        $order->addListservice($service);

        $em->persist($service);
        $em->flush();

        return new Response();
    }

    //output services of the order
    /**
     * @Route("/orders/{id}/services", name="orders_services", methods={"GET"})
     */
    public function getOrderServices(int $id, EntityManagerInterface $em): Response
    {
        $order = $em->getRepository(Order::class)->find($id);

        foreach ($order->getListservices() as $service){
            dd($service);
        }

        return new Response();
    }

}