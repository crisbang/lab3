<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Service;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ServiceController extends AbstractController
{
    /**
     * @Route("/services/create", name="service_create", methods={"GET"})
     */
    public function create(ServiceRepository $repository): Response
    {
        $service = new Service();
        $service
            ->setTitle('Прокол индастриал')
            ->setDescription('Украшение соединяет сразу два прокола во внешнем и внутреннем краях уха');

        $repository->save($service, true);
        return new Response();
    }

    /**
     * @Route("/services/update", name="service_update", methods={"GET"})
     */
    public function update(ServiceRepository $repository): Response
    {
        $service = $repository->find(1);

        if ($service !== null) {
            $service->setDescription('Украшение соединяет сразу два прокола во внешнем и внутреннем краях уха. Уровень боли: 6 из 10');
            $repository->save($service, true);
        }

        return new Response();
    }

    /**
     * @Route("/services/remove", name="service_remove", methods={"GET"})
     */
    public function remove(ServiceRepository $repository): Response
    {
        $service = $repository->find(1);

        if ($service !== null) {
            $repository->remove($service, true);
        }

        return new Response();
    }

}