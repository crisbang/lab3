<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/products/create", name="product_create", methods={"GET"})
     */
    public function create(ProductRepository $repository): Response
    {
        $product = new Product();
        $product
            ->setTitle('Штанга Индастриал')
            ->setPrice(350)
            ->setDescription('Материал: титан');

        $repository->save($product, true);
        return new Response();
    }

    /**
     * @Route("/products/update", name="product_update", methods={"GET"})
     */
    public function update(ProductRepository $repository): Response
    {
        $product = $repository->find(1);

        if ($product !== null) {
            $product->setPrice(950);
            $repository->save($product, true);
        }

        return new Response();
    }

    /**
     * @Route("/products/remove", name="product_remove", methods={"GET"})
     */
    public function remove(ProductRepository $repository): Response
    {
        $product = $repository->find(2);

        if ($product !== null) {
            $repository->remove($product, true);
        }

        return new Response();
    }

}