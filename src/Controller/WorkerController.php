<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Worker;
use App\Repository\WorkerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WorkerController extends AbstractController
{
    /**
     * @Route("/workers/update", name="worker_update", methods={"GET"})
     */
    public function update(WorkerRepository $repository): Response
    {
        $worker = $repository->find(1);

        if ($worker !== null) {
            $worker->setPhone('89998765432');
            $repository->save($worker, true);
        }

        return new Response();
    }

}