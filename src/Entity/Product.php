<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 45)]
    private ?string $title = null;

    #[ORM\Column(nullable: true)]
    private ?int $price = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\ManyToMany(targetEntity: Order::class, mappedBy: 'listproducts')]
    private Collection $ordersprod;

    public function __construct()
    {
        $this->ordersprod = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrdersprod(): Collection
    {
        return $this->ordersprod;
    }

    public function addOrdersprod(Order $ordersprod): self
    {
        if (!$this->ordersprod->contains($ordersprod)) {
            $this->ordersprod->add($ordersprod);
            $ordersprod->addListproduct($this);
        }

        return $this;
    }

    public function removeOrdersprod(Order $ordersprod): self
    {
        if ($this->ordersprod->removeElement($ordersprod)) {
            $ordersprod->removeListproduct($this);
        }

        return $this;
    }
}
