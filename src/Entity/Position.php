<?php

namespace App\Entity;

use App\Repository\PositionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PositionRepository::class)]
class Position
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 45)]
    private ?string $title = null;

    #[ORM\ManyToMany(targetEntity: Service::class, inversedBy: 'positions')]
    private Collection $services;

    #[ORM\OneToMany(mappedBy: 'position', targetEntity: Worker::class)]
    private Collection $workerspos;

    public function __construct()
    {
        $this->services = new ArrayCollection();
        $this->workerspos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, Service>
     */
    public function getServices(): Collection
    {
        return $this->services;
    }

    public function addService(Service $service): self
    {
        if (!$this->services->contains($service)) {
            $this->services->add($service);
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        $this->services->removeElement($service);

        return $this;
    }

    /**
     * @return Collection<int, Worker>
     */
    public function getWorkerspos(): Collection
    {
        return $this->workerspos;
    }

    public function addWorkerspo(Worker $workerspo): self
    {
        if (!$this->workerspos->contains($workerspo)) {
            $this->workerspos->add($workerspo);
            $workerspo->setPosition($this);
        }

        return $this;
    }

    public function removeWorkerspo(Worker $workerspo): self
    {
        if ($this->workerspos->removeElement($workerspo)) {
            // set the owning side to null (unless already changed)
            if ($workerspo->getPosition() === $this) {
                $workerspo->setPosition(null);
            }
        }

        return $this;
    }
}
