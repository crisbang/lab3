<?php

namespace App\Entity;

use App\Repository\WorkerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WorkerRepository::class)]
class Worker
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $fio = null;

    #[ORM\Column(length: 11, nullable: true)]
    private ?string $phone = null;

    #[ORM\OneToMany(mappedBy: 'orworker', targetEntity: Order::class)]
    private Collection $orworkers;

    #[ORM\ManyToOne(inversedBy: 'workerspos')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Position $position = null;

    public function __construct()
    {
        $this->orworkers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrworkers(): Collection
    {
        return $this->orworkers;
    }

    public function addWorder(Order $worder): self
    {
        if (!$this->orworkers->contains($worder)) {
            $this->orworkers->add($worder);
            $worder->setOrworker($this);
        }

        return $this;
    }

    public function removeWorder(Order $worder): self
    {
        if ($this->orworkers->removeElement($worder)) {
            // set the owning side to null (unless already changed)
            if ($worder->getOrworker() === $this) {
                $worder->setOrworker(null);
            }
        }

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }
}
