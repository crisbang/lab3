<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: '`order`')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20)]
    private ?string $date = null;

    #[ORM\Column(nullable: true)]
    private ?int $cost = null;

    #[ORM\ManyToMany(targetEntity: Service::class, inversedBy: 'ordersserv')]
    private Collection $listservices;

    #[ORM\ManyToMany(targetEntity: Product::class, inversedBy: 'ordersprod')]
    private Collection $listproducts;

    #[ORM\ManyToOne(inversedBy: 'corders')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Client $client = null;

    #[ORM\ManyToOne(inversedBy: 'orworkers')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Worker $orworker = null;

    public function __construct()
    {
        $this->listservices = new ArrayCollection();
        $this->listproducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(?int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * @return Collection<int, Service>
     */
    public function getListservices(): Collection
    {
        return $this->listservices;
    }

    public function addListservice(Service $listservice): self
    {
        if (!$this->listservices->contains($listservice)) {
            $this->listservices->add($listservice);
        }

        return $this;
    }

    public function removeListservice(Service $listservice): self
    {
        $this->listservices->removeElement($listservice);

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getListproducts(): Collection
    {
        return $this->listproducts;
    }

    public function addListproduct(Product $listproduct): self
    {
        if (!$this->listproducts->contains($listproduct)) {
            $this->listproducts->add($listproduct);
        }

        return $this;
    }

    public function removeListproduct(Product $listproduct): self
    {
        $this->listproducts->removeElement($listproduct);

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getOrworker(): ?Worker
    {
        return $this->orworker;
    }

    public function setOrworker(?Worker $orworker): self
    {
        $this->orworker = $orworker;

        return $this;
    }
}
