<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $fio = null;

    #[ORM\Column(length: 11, nullable: true)]
    private ?string $phone = null;

    #[ORM\Column(length: 25, nullable: true)]
    private ?string $email = null;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: Order::class)]
    private Collection $corders;

    public function __construct()
    {
        $this->corders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFio(): ?string
    {
        return $this->fio;
    }

    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getCorders(): Collection
    {
        return $this->corders;
    }

    public function addCorder(Order $corder): self
    {
        if (!$this->corders->contains($corder)) {
            $this->corders->add($corder);
            $corder->setClient($this);
        }

        return $this;
    }

    public function removeCorder(Order $corder): self
    {
        if ($this->corders->removeElement($corder)) {
            // set the owning side to null (unless already changed)
            if ($corder->getClient() === $this) {
                $corder->setClient(null);
            }
        }

        return $this;
    }
}
