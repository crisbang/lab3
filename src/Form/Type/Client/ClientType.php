<?php

declare(strict_types=1);

namespace App\Form\Type\Client;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fio', TextType::class, [
                'label' => 'Ф.И.О.',
                'constraints' => [new Assert\NotBlank(), new Assert\Length(max:255)],
                'attr' => ['class' => 'form_input', 'placeholder' => 'Имя*']

            ])

            ->add('phone', TextType::class, [
                'label' => 'Телефон',
                'constraints' => [new Assert\NotBlank(), new Assert\Length(max:11)],
                'attr' => ['class' => 'form_input', 'placeholder' => 'Телефон*']
            ])

            ->add('email', TextType::class, [
                'label' => 'Email',
                'constraints' => [new Assert\NotBlank(), new Assert\Length(max:25)],
                'attr' => ['class' => 'form_input', 'placeholder' => 'Email']

            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Client::class,]);
    }

}