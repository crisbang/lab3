<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230111133532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add a relationship between tables position and worker';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE worker ADD position_id INT NOT NULL');
        $this->addSql('ALTER TABLE worker ADD CONSTRAINT FK_9FB2BF62DD842E46 FOREIGN KEY (position_id) REFERENCES position (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9FB2BF62DD842E46 ON worker (position_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE worker DROP CONSTRAINT FK_9FB2BF62DD842E46');
        $this->addSql('DROP INDEX IDX_9FB2BF62DD842E46');
        $this->addSql('ALTER TABLE worker DROP position_id');
    }
}
