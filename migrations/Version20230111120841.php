<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230111120841 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create order_service table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE order_service (order_id INT NOT NULL, service_id INT NOT NULL, PRIMARY KEY(order_id, service_id))');
        $this->addSql('CREATE INDEX IDX_17E733998D9F6D38 ON order_service (order_id)');
        $this->addSql('CREATE INDEX IDX_17E73399ED5CA9E6 ON order_service (service_id)');
        $this->addSql('ALTER TABLE order_service ADD CONSTRAINT FK_17E733998D9F6D38 FOREIGN KEY (order_id) REFERENCES "order" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_service ADD CONSTRAINT FK_17E73399ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE order_service DROP CONSTRAINT FK_17E733998D9F6D38');
        $this->addSql('ALTER TABLE order_service DROP CONSTRAINT FK_17E73399ED5CA9E6');
        $this->addSql('DROP TABLE order_service');
    }
}
