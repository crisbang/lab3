<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230111091703 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create Client';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE order_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE position_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE service_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE worker_id_seq CASCADE');
        $this->addSql('ALTER TABLE worker DROP CONSTRAINT fk_9fb2bf62dd842e46');
        $this->addSql('ALTER TABLE position_service DROP CONSTRAINT fk_726c8b21dd842e46');
        $this->addSql('ALTER TABLE position_service DROP CONSTRAINT fk_726c8b21ed5ca9e6');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE "order"');
        $this->addSql('DROP TABLE worker');
        $this->addSql('DROP TABLE service');
        $this->addSql('DROP TABLE position_service');
        $this->addSql('DROP TABLE position');
        $this->addSql('ALTER TABLE client DROP message');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE order_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE position_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE service_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE worker_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, title VARCHAR(45) NOT NULL, price INT DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "order" (id INT NOT NULL, date DATE NOT NULL, cost INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE worker (id INT NOT NULL, position_id INT NOT NULL, fio VARCHAR(255) NOT NULL, phone VARCHAR(11) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_9fb2bf62dd842e46 ON worker (position_id)');
        $this->addSql('CREATE TABLE service (id INT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE position_service (position_id INT NOT NULL, service_id INT NOT NULL, PRIMARY KEY(position_id, service_id))');
        $this->addSql('CREATE INDEX idx_726c8b21ed5ca9e6 ON position_service (service_id)');
        $this->addSql('CREATE INDEX idx_726c8b21dd842e46 ON position_service (position_id)');
        $this->addSql('CREATE TABLE position (id INT NOT NULL, title VARCHAR(45) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE worker ADD CONSTRAINT fk_9fb2bf62dd842e46 FOREIGN KEY (position_id) REFERENCES "position" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE position_service ADD CONSTRAINT fk_726c8b21dd842e46 FOREIGN KEY (position_id) REFERENCES "position" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE position_service ADD CONSTRAINT fk_726c8b21ed5ca9e6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE client ADD message VARCHAR(255) DEFAULT NULL');
    }
}
