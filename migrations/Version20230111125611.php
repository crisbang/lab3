<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230111125611 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add a relationship between tables order and worker';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "order" ADD orworker_id INT NOT NULL');
        $this->addSql('ALTER TABLE "order" ADD CONSTRAINT FK_F5299398D73E282F FOREIGN KEY (orworker_id) REFERENCES worker (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F5299398D73E282F ON "order" (orworker_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "order" DROP CONSTRAINT FK_F5299398D73E282F');
        $this->addSql('DROP INDEX IDX_F5299398D73E282F');
        $this->addSql('ALTER TABLE "order" DROP orworker_id');
    }
}
