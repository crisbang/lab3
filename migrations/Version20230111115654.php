<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230111115654 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'create position_service table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE position_service (position_id INT NOT NULL, service_id INT NOT NULL, PRIMARY KEY(position_id, service_id))');
        $this->addSql('CREATE INDEX IDX_726C8B21DD842E46 ON position_service (position_id)');
        $this->addSql('CREATE INDEX IDX_726C8B21ED5CA9E6 ON position_service (service_id)');
        $this->addSql('ALTER TABLE position_service ADD CONSTRAINT FK_726C8B21DD842E46 FOREIGN KEY (position_id) REFERENCES position (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE position_service ADD CONSTRAINT FK_726C8B21ED5CA9E6 FOREIGN KEY (service_id) REFERENCES service (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE position_service DROP CONSTRAINT FK_726C8B21DD842E46');
        $this->addSql('ALTER TABLE position_service DROP CONSTRAINT FK_726C8B21ED5CA9E6');
        $this->addSql('DROP TABLE position_service');
    }
}
